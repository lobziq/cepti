package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageWikiArticle {
    private By locatorArticleTitle = By.id("firstHeading");
    private WebDriver driver;

    public PageWikiArticle(WebDriver driver) {
        this.driver = driver;
    }

    public String getArticleTitle() {
        return driver.findElement(locatorArticleTitle).getText();
    }
}
