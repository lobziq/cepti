package tests;

import Pages.PageWikiArticle;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestWiki {
    private WebDriver driver;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\lobzi\\IdeaProjects\\cepti\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void testPageTitle() {
        driver.get("https://en.wikipedia.org/wiki/Vampire:_The_Masquerade");
        Assert.assertEquals(driver.getTitle(), "Vampire: The Masquerade - Wikipedia");
    }

    @Test
    public void testPageHeader() {
        driver.get("https://en.wikipedia.org/wiki/Vampire:_The_Masquerade");
        PageWikiArticle p = new PageWikiArticle(driver);
        Assert.assertEquals(p.getArticleTitle(), "Vampire: The Masquerade - Wikipedia");
    }

    @AfterTest
    public void tearDown() {
        driver.close();
    }
}
